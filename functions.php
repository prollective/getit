<?php
/**
 * GetIt functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GetIt
 */

if ( ! function_exists( 'getit_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function getit_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on GetIt, use a find and replace
	 * to change 'getit' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'getit', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'getit' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'getit_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'getit_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function getit_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'getit_content_width', 640 );
}
add_action( 'after_setup_theme', 'getit_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function getit_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'getit' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'getit' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'getit_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function getit_scripts() {
	wp_enqueue_style( 'getit-style', get_stylesheet_uri() );

  wp_enqueue_script( 'getit-modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false );

  wp_enqueue_script( 'getit-main', get_template_directory_uri() . '/js/main.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'getit_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Enqueue Google Fonts using a function
 */
function custom_enqueue_google_font() {

	// Setup font arguments
	$query_args = array(
		'family' => 'Open+Sans:400, 700' // Change this font to whatever font you'd like
	);

 	// A safe way to register a CSS style file for later use
	wp_register_style( 'google-fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );

	// A safe way to add/enqueue a CSS style file to a WordPress generated page
	wp_enqueue_style( 'google-fonts' );
}

add_action( 'wp_enqueue_scripts', 'custom_enqueue_google_font' );

// Custom Backend Footer
function website_custom_admin_footer() {
	_e('<span id="footer-thankyou">Developed by <a href="http://prollective.com" target="_blank">Prollective</a></span>', 'website');
}

// Add it to the admin area
add_filter('admin_footer_text', 'website_custom_admin_footer');

function generate_voucher_validator($result, $tag) {
  $voucherKey = '72667AE08AD1B57169336CF2113C1129';
  $formTag = new WPCF7_FormTag($tag);
  if ('mobilnummer' == $formTag->name) {
    $eventDate = $_POST['kampdag'];
    $phone = $_POST['mobilnummer'];

    if ($phone == '' || $eventDate == '') {
      $result->invalidate($tag, 'Du har ikke indtastet dit mobilnummer og valgt en Champions League aften');
      return $result;
    }


    switch ($eventDate) {
      case '11 april':
        $voucherId = 320;
        break;
      case '12 april':
        $voucherId = 321;
        break;
      case '18 april':
        $voucherId = 327;
        break;
      case '19 april':
        $voucherId = 328;
        break;
      case '2 maj':
        $voucherId = 329;
        break;
      case '3 maj':
        $voucherId = 330;
        break;
    }

    $date = date('Y-m-d H:i', strtotime('+2 hours'));
    $tokenPart = strtoupper(md5('Mobile:' . $phone . 'VoucherTypeId:' . $voucherId . 'Date:' . $date . 'Version:1'));
    $token = md5($tokenPart . $voucherKey);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://getit.mph1.com/ExtAPI/CreateVoucher");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
      "Mobile" => $phone,
      "Date" => $date,
      "VoucherTypeId" => $voucherId
    )));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "X-MP-Auth: " . strtoupper($token),
      'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    if ($status != 200) {
      $result->invalidate($tag, 'Du skal udfylde dit mobilnummer');
      return $result;
    }

    $data = json_decode($response);
    if ($data->Result != 'OK') {
      $result->invalidate($tag, 'Du skal udfylde dit mobilnummer');
    }

  }
  return $result;
}
add_filter('wpcf7_validate_text*', 'generate_voucher_validator', 20, 2);
