<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

<script>
  // https://contactform7.com/redirecting-to-another-url-after-submissions/
  document.addEventListener( 'wpcf7mailsent', function( event ) {
    var eventday = jQuery('.wpcf7 select[name=kampdag]').val()
    location = '/confirmation/?eventday='+eventday+'';
  }, false );
</script>  

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'pagehome' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			
		</main><!-- #main -->
	</div><!-- #primary -->

    
<?php 
  
  $limit = 15;
  
  /* 
    Dates 11 & 12 april
  */
  $data_11_april_string = do_shortcode("[cfdb-count form='Form' filter='kampdag=11 april' role='Anyone']");
  $data_11_april = (string) $data_11_april_string; // Casts to string
  // echo $data_11_april;
  
  $data_12_april_string = do_shortcode("[cfdb-count form='Form' filter='kampdag=12 april' role='Anyone']");
  $data_12_april = (string) $data_12_april_string; // Casts to string
  // echo $data_12_april;
  
  if ($data_11_april >= $limit) { ?> <script>jQuery(function() { jQuery('.wpcf7 .kampdag select option[value="11 april"]').hide().remove(); });</script> <?php }
  if ($data_12_april >= $limit) { ?> <script>jQuery(function() { jQuery('.wpcf7 .kampdag select option[value="12 april"]').hide().remove(); });</script> <?php }
    
  if ( ( ($data_11_april) >= $limit) && ( ($data_12_april) >= $limit) ) { ?>
    <script>
      jQuery(function() {
        jQuery('<p class="form-closed">Der er fuldt booket, men der vil snart blive åbnet op for nye events.<br>Download vores App og få besked når det sker.</p>').insertBefore('.wpcf7');
        jQuery('.home .cta').show();
        jQuery('.wpcf7').remove();
      });
      
    </script>
  <?php } ?>
  
  
<?php
  
  /* 
    Dates 18 & 19 april
  */
  $data_18_april_string = do_shortcode("[cfdb-count form='Form2' filter='kampdag=18 april' role='Anyone']");
  $data_18_april = (string) $data_18_april_string; // Casts to string
  // echo $data_18_april;
  
  $data_19_april_string = do_shortcode("[cfdb-count form='Form2' filter='kampdag=19 april' role='Anyone']");
  $data_19_april = (string) $data_19_april_string; // Casts to string
  // echo $data_19_april;
  
  if ($data_18_april >= $limit) { ?> <script>jQuery('.wpcf7 .kampdag select option[value="18 april"]').hide().remove();</script> <?php }
  if ($data_19_april >= $limit) { ?> <script>jQuery('.wpcf7 .kampdag select option[value="19 april"]').hide().remove();</script> <?php }
    
  if ( ( ($data_18_april) >= $limit) && ( ($data_19_april) >= $limit) ) { ?>
    <script>
      jQuery('<p class="form-closed">Der er fuldt booket, men der vil snart blive åbnet op for nye events. Download vores App og få besked når det sker.</p>').insertBefore('.wpcf7');
      jQuery('.home .cta').show();
      jQuery('.wpcf7').remove();
    </script>
  <?php } ?>  


<?php
  
  /* 
    Dates 2 & 3 maj
  */
  $data_2_maj_string = do_shortcode("[cfdb-count form='Form3' filter='kampdag=2 maj' role='Anyone']");
  $data_2_maj = (string) $data_2_maj_string; // Casts to string
  // echo $data_2_maj;
  
  $data_3_maj_string = do_shortcode("[cfdb-count form='Form3' filter='kampdag=3 maj' role='Anyone']");
  $data_3_maj = (string) $data_3_maj_string; // Casts to string
  // echo $data_3_maj;
  
  if ($data_2_maj >= $limit) { ?> <script>jQuery('.wpcf7 .kampdag select option[value="2 maj"]').hide().remove();</script> <?php }
  if ($data_3_maj >= $limit) { ?> <script>jQuery('.wpcf7 .kampdag select option[value="3 maj"]').hide().remove();</script> <?php }
    
  if ( ( ($data_2_maj) >= $limit) && ( ($data_3_maj) >= $limit) ) { ?>
    <script>
      jQuery('<p class="form-closed">Der er fuldt booket, men der vil snart blive åbnet op for nye events. Download vores App og få besked når det sker.</p>').insertBefore('.wpcf7');
      jQuery('.home .cta').show();
      jQuery('.wpcf7').remove();
    </script>
  <?php } ?>


  

<?php
  
/*
require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension-master/CFDBFormIterator.php');
$exp = new CFDBFormIterator();
$atts = array();
$atts['filter'] = "kampdag=11 april";
$exp->export($atts['Form'], $atts);
$i = 0;
while ($row = $exp->nextRow()) {
 // Add your stuff here!
   $i++;
}
echo $i;
*/


?>



<?php get_footer(); ?>