<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GetIt
 */

?>

<div class="confirmation">
  
  <?php the_title( '<div class="title">', '</div>' ); ?> 
  
  <div class="content">
    <?php the_content(); ?>
  </div>
  
  <div class="iphone">
    <img src="<?php echo get_template_directory_uri(); ?>/img/iphone.png" />
  </div>
  
</div>

  <?php
    $eventday = htmlspecialchars($_GET["eventday"]);
    if ($eventday) { ?>
      <script>
        jQuery('#eventdate').text('<?php echo $eventday; ?>');
      </script>
    <?php } ?>

