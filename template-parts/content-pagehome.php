<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GetIt
 */

?>


<div class="topbanner">
  <?php the_title( '<div class="title">', '</div>' ); ?>
  
  <div class="image">
    <img src="<?php echo get_template_directory_uri(); ?>/img/banner.jpg" />
  </div>
  
  <div class="content-wrapper">
    <div class="content">
      <?php the_content(); ?>
    </div>
    
    <div class="cta" style="display: none;">
      <div class="getit">
         <img src="<?php echo get_template_directory_uri(); ?>/img/getit-black.png" />
      </div>
      <div class="links">
        <a href="https://play.google.com/store/apps/details?id=dk.mobilepeople.getit" target="_blank" class="first">
          <img src="<?php echo get_template_directory_uri(); ?>/img/google.png" />
        </a>
        <a href="https://itunes.apple.com/dk/app/get-it/id1130929381?l=da&mt=8" target="_blank">
          <img src="<?php echo get_template_directory_uri(); ?>/img/apple.png" />
        </a>
      </div>
    </div>
    
  </div>  
</div>
